"""VM Manager

Description:
This script includes a set of classes to deal with VM's in AWS.
It will use the credentials file set in credfilename which if a global variable.
execute must be called with:
    action: 
        'start' starts VM's
        'stop'  stops VM's
    vmfilter:
        Example: 'pepe', let's you specify a filter for the VM's whose name tag includes this string
    proxy:
        'y'|'Y'|'n'|'N', indicates whether to use proxy or not. The proxy must be configured in the credentials file
        
The script also let's you create lists of VM's based on a filter.
You can also assign an Elastic IP to a VM. 
The VM needs to have a tag with key 'IPAddress' set to the elastic/public IP to assign.

Author:
   Rodolfo Kohn - rodolk@yahoo.com

Creation date: 2013-12-12

"""

import boto
import time
import re
import sys
import xml.etree.ElementTree as ET


credfilename = None


def getCleanLine(line):
    eol = 0
    if line[(eol-1)]=='\n' or line[(eol-1)]=='\r':
        eol = -1
    if line[(eol-1)]=='\n' or line[(eol-1)]=='\r':
        eol = -2
    if eol < 0:
        return line[:eol]
    else:
        return line

def addDictKey(argDict, line):
    linetup = line.partition('=')
    argDict[linetup[0]] = linetup[2]
        
        
class AWSSettings:
    def __init__(self, credfile):
        print "FILE %s" % credfile
        self.file_parameters = {}
        if credfile:
            f = open(credfile, 'r')
            if f:
                addDictKey(self.file_parameters, getCleanLine(f.readline()))
                addDictKey(self.file_parameters, getCleanLine(f.readline()))
                addDictKey(self.file_parameters, getCleanLine(f.readline()))
                addDictKey(self.file_parameters, getCleanLine(f.readline()))
                
                print self.file_parameters
                
                self.accessKeyID = self.file_parameters['ACCESS_KEY_ID']
                self.secretKey = self.file_parameters['SECRET_KEY']
                self.AWS_PROXY = self.file_parameters['PROXY_SERVER'];
                self.AWS_PROXY_PORT = self.file_parameters['PROXY_PORT'];

                print "Access Key ID: %s" % self.accessKeyID
                print "Secret Key: %s" % self.secretKey
                print "PROXY: %s" % self.AWS_PROXY
                print "PORT: %s" % self.AWS_PROXY_PORT
                
                f.close()
            else:
                print "Error opening credentials file"

            
            
        
        
class ManagerEC2:
    def __init__(self, settings, proxy=None):
        print "Connecting to EC2"
        self.settings = settings
        if not proxy or proxy=='N' or proxy=='n':
            print "KEY ID %s" % settings.accessKeyID
            print "KEY ID %s" % settings.secretKey
            self.ec2 = boto.connect_ec2(settings.accessKeyID, settings.secretKey)
        else:
            print "USE Proxy"
            self.ec2 = boto.connect_ec2(settings.accessKeyID, settings.secretKey
                                        ,proxy=settings.AWS_PROXY
                                        ,proxy_port=settings.AWS_PROXY_PORT
                                       )
        print "Connected to EC2"
        
    def startVMs(self, VMList):
        instanceIDList = None
        
        for i in VMList:
            if instanceIDList:
                instanceIDList = instanceIDList + [i.getID()];
            else:
                instanceIDList = [i.getID()]
        
        if instanceIDList:
            return self.ec2.start_instances(instanceIDList)
        else:
            return None
        
    def stopVMs(self, VMList):
        instanceIDList = None
        
        for i in VMList:
            if instanceIDList:
                instanceIDList = instanceIDList + [i.getID()];
            else:
                instanceIDList = [i.getID()]
        
        if instanceIDList:
            return self.ec2.stop_instances(instanceIDList)
        else:
            return None
        
    
    def instantiate(self, vm):
        print vm.imageID
        print vm.keyName
        e = self.ec2
        reservation = e.run_instances(image_id=vm.imageID, key_name=vm.keyName)
        return reservation
        
    def showAllInstances(self, reservation):
        e = self.ec2
        for r in e.get_all_instances():
            print r.instances[0].public_dns_name

#            if r.id == reservation.id:
#                break

    def getInstance(self, insID):
        mylist = [insID]
        for r in self._ec2Conn.get_all_instances(instance_ids=mylist):
            return r.instances[0]

    def getVMList(self, VMFilter=None):
        e = self.ec2
        
        reservation = e.get_all_instances()
        listVirtualMachines = ListVirtualMachines(self)

        for r in reservation:
            for i in r.instances:
                print "-----Instance ID: %s" % i.id
                vm = VirtualMachine(self, i)
                if VMFilter:
                    if VMFilter.evaluate(i):
                        listVirtualMachines.add(vm)
                else:
                    listVirtualMachines.add(vm)

        return listVirtualMachines
            
            
class VirtualMachine:
    def __init__(self, mgrEC2=None, instance=None):
        self.mgrEC2 = mgrEC2
        self.instance = instance
        
    def getID(self):
        if self.instance:
            return self.instance.id
        else:
            return None
            
    def getPublicIP(self):
        if self.instance:
            return self.instance.ip_address
        else:
            return None
            
    def getPrivateIP(self):
        if self.instance:
            return self.instance.private_ip_address
        else:
            return None
            
    def getPublicDNS(self):
        if self.instance:
            return self.instance.public_dns_name
        else:
            return None
            
    def getPrivateDNS(self):
        if self.instance:
            return self.instance.private_dns_name
        else:
            return None
            
    def getName(self):
        if self.instance:
            return self.instance.__dict__['tags']['Name']
        else:
            return None
            
    def getTaggedElasticIP(self):
        if self.instance:
            return self.instance.__dict__['tags']['IPAddress']
        else:
            return None
     
    def assignPublicIP(self, ipaddress=None):
        if not ipaddress:
            ipaddress = self.instance.__dict__['tags']['IPAddress']
        print "IP: %s" % ipaddress
        print "ID: %s" % self.instance.id
        eipaddress = boto.ec2.address.Address(ipaddress)
        self.mgrEC2.ec2.associate_address(self.instance.id, ipaddress)
        
#    def initialize(self, size, imageID, keyName):
    
#    def create(self, size, imageID, keyName):
#        self.size = size
#        self.imageID = imageID
#        self.keyName = keyName
#        reservation = self.mgrEC2.instantiate(self)
#        return reservation

        
class ListVirtualMachines:
    def __init__(self, mgrEC2=None):
        self.mgrEC2 = mgrEC2
        self.vmlist = []
        
    def add(self, vm):
        if self.vmlist:
            self.vmlist = self.vmlist + [vm]
        else:
            self.vmlist = [vm]
        
    def remove(self, vm):
        if self.vmlist:
            idx = self.vmlist.index(vm)
            if  idx > -1:
                self.vmlist[idx:idx+1] = []
    
    def start(self):
        if self.vmlist:
            self.mgrEC2.startVMs(self.vmlist)
    
    def stop(self):
        if self.vmlist:
            self.mgrEC2.stopVMs(self.vmlist)
    
    def getList(self):
        return self.vmlist

            
class OpenCloudFilter:
    def __init__(self, regexpString):
                self._regexpString = regexpString

class OpenCloudVMFilter(OpenCloudFilter):
    def __init__(self, regexpString):
        self._regexpString = regexpString
                
    def evaluate(self, vm):
		
        matchObj = re.match(self._regexpString, vm.__dict__['tags']['Name'])
        if matchObj:
            return vm
        else:
            return None

class VMListDescription:
    def __init__(self, _vmlist, _xml):
        self.vmlist = _vmlist
        self.xml = _xml

def help():
    print "You are using bad this script. This is the way:"
    print "managevm start|stop [vmfilter=string] [proxy=Y|y|YES|yes]"
    
def executeStart(mgrEC2, vmList, appliedFilter):
    rawvmlist = vmList.getList()
    
    print "VM's to start:"
    
    vmXMLRoot = ET.Element('VirtualMachine')
    for i in rawvmlist:
        attr = {'id': i.getID(), 'name': i.getName()}
        ET.SubElement(vmXMLRoot, 'Instance', attr)

    ET.dump(vmXMLRoot)

    vmList.start()

    print "Wait 15 seconds to continue"
    time.sleep(15)

    vmList = mgrEC2.getVMList(appliedFilter)
    rawvmlist = vmList.getList()
    
    print "Started VM's:"
    
    vmXMLRoot = ET.Element('VirtualMachine')

    for i in rawvmlist:
        attr = {'id': i.getID(), 'name': i.getName(), 'state': i.instance.state, 'statecode': str(i.instance.state_code)}
        ET.SubElement(vmXMLRoot, 'Instance', attr)
        print "Instance ID: %s" % i.getID()
        print "Instance Name: %s" % i.getName()
        print "State: %s" % i.instance.state
        print "State code: %s" % i.instance.state_code
        print "Previous state: %s" % i.instance.previous_state
        print "Instance Public IP: %s" % i.getPublicIP()
        print "Instance Private IP: %s" % i.getPrivateIP()
        print "Instance Public DNS: %s" % i.getPublicDNS()
        print "Instance Private DNS: %s" % i.getPrivateDNS()

    ET.dump(vmXMLRoot)
    
    return VMListDescription(vmList, vmXMLRoot)

def executeStop(mgrEC2, vmList, appliedFilter):
    print "Stopping VM's"
    vmList.stop()

    print "Wait 15 seconds to continue"
    time.sleep(15)

    vmList = mgrEC2.getVMList(appliedFilter)
    rawvmlist = vmList.getList()
    
    print "Stopped VM's:"
    vmXMLRoot = ET.Element('VirtualMachine')
    for i in rawvmlist:
        attr = {'id': i.getID(), 'name': i.getName(), 'state': i.instance.state, 'statecode': str(i.instance.state_code)}
        ET.SubElement(vmXMLRoot, 'Instance', attr)

    ET.dump(vmXMLRoot)
    
    return VMListDescription(vmList, vmXMLRoot)

    
def execute(action, vmfilter=None, proxy=None):
    print "BEGINNING"
    
    if action != 'start' and action != 'stop':
       print "ERROR: action not valid\n"
       help()
       sys.exit(1)

    if proxy and (proxy != 'y' and proxy != 'Y' and proxy != 'N' and proxy != 'n'):
       print "ERROR: proxy not valid\n"
       help()
       sys.exit(1)
       
    awsSettings = AWSSettings(credfilename)
    mgrEC2 = ManagerEC2(awsSettings, proxy)

    if not mgrEC2:
       print "ERROR: Conecting to EC2\n"
       sys.exit(1)

    if vmfilter:
        print "Using filter: %s\n" % vmfilter

    vmfilter = '.*' + vmfilter + '.*'
    appliedFilter = OpenCloudVMFilter(vmfilter)
    vmList = mgrEC2.getVMList(appliedFilter)
    
    vmListdesc = None
    
    if action=="start":
        vmListdesc = executeStart(mgrEC2, vmList, appliedFilter)
    else:
        if action=="stop":
            vmListdesc = executeStop(mgrEC2, vmList, appliedFilter)
        else:
            print "wrong action\n"
            sys.exit(1)

    return vmListdesc

if __name__ == "__main__":
    comm = None
    fil = None
    proxy = None
    
    if len(sys.argv) != 5:
        print "wrong number of arguments\n"
        sys.exit(1)
    
    for a in sys.argv:
        print "ARG: %s" % a
    
    if sys.argv[1]:
        comm = sys.argv[1]
    else:
        print "wrong no action\n"
        sys.exit(1)
        
    if sys.argv[2]:
        fil = sys.argv[2]
    else:
        print "wrong no filter\n"
        sys.exit(1)
        
    if sys.argv[3]:
        proxy = sys.argv[3]
    else:
        print "wrong no proxy setting\n"
        sys.exit(1)

    if sys.argv[4]:
        credfilename = sys.argv[4]
    else:
        print "wrong no credentials file\n"
        sys.exit(1)
        
    execute(comm, fil, proxy)

    
    
