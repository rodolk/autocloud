#!python
"""Example of AWS VM manager

Description:
This script uses managevm to start or stop the VM's whose name tag matches vmfilter.
It works with or w/o proxy and the specified credentials file.
Proxy server and port must be specified by in the .cred file and is going to be used by managevm
When VM's are started it stores public IP addresses in a file called vmdata.txt
It allows assignment of Elastic IP's to VM's in the following mode:
   The VM needs to have a tag with key 'IPAddress' set to the elastic/public IP to assign.
   That elastic IP is read and assigned to the VM after starting said VM.

Author:
   Rodolfo Kohn - rodolk@yahoo.com

Creation date: 2013-12-12

"""

import boto
import time
import re
import sys
import managevm
   
def execute(action, vmfilter=None, proxy=None, credfilename=None, outmode=None):
    listip = ""
    first = 1
    outfile = None
    print "BEGIN"
    
    if credfilename != None:
        managevm.credfilename = credfilename;
    else:
        managevm.credfilename = 'credentials_myproject.cred';
        
    vmListDesc = managevm.execute(action, vmfilter, proxy)
    print "EXECUTE DONE"

    if action == 'start':
        if (outmode != None and outmode == 'cont'):
            print 'APEND %s\n' % outmode
            outfile = open('vmdata.txt', 'a')
        else:
            print 'NEW %s\n' % outmode
            outfile = open('vmdata.txt', 'w')

        vmObjlist = vmListDesc.vmlist.getList()

        for i in vmObjlist:
            try:
                iptoassociate = i.getTaggedElasticIP()
                i.assignPublicIP()
            except KeyError:
                iptoassociate = i.getPublicIP()
            
            if first:
                listip = iptoassociate
                first = None
            else:
                listip = listip + "," + iptoassociate

        print "List: %s" % listip
        listip = 'My VM\'s=' + listip
        outfile.write(listip + '\n');
        outfile.close()
    
if __name__ == "__main__":
    comm = None
    fil = None
    proxy = None
    credfilename = None
    outmode = None
    
    if len(sys.argv) < 5:
        print "wrong number of arguments\n"
        sys.exit(1)
    
    for a in sys.argv:
        print "ARG: %s" % a
    
    if sys.argv[1]:
        comm = sys.argv[1]
    else:
        print "wrong no action\n"
        sys.exit(1)
        
    if sys.argv[2]:
        fil = sys.argv[2]
    else:
        print "wrong no filter\n"
        sys.exit(1)
        
    if sys.argv[3]:
        proxy = sys.argv[3]
    else:
        print "wrong no proxy setting\n"
        sys.exit(1)

    if sys.argv[4]:
        credfilename = sys.argv[4]
        
    if len(sys.argv) == 6 and sys.argv[5]:
        outmode = sys.argv[5]
        
    execute(comm, fil, proxy, credfilename, outmode)

        
        
        
