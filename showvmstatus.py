#!python
"""Show VM status

Description:
This script uses managevm to show your VM's status.
It works with or w/o proxy and the specified credentials file. It can use a filter.
It generates output in plain text and xml format. 
The objective is to easily detect VM's you forgot running and are increasing your bill.
It can display status for all VM's or only running and pending VM's.
Proxy server and port must be specified by in the .cred file and is going to be used by managevm

Author:
   Rodolfo Kohn - rodolk@yahoo.com

Creation date: 2014-01-10

"""

import boto
import time
import re
import sys
import managevm
import xml.etree.ElementTree as ET


def execute(proxy=None, credfilename=None, vmfilter=None, format=None, status=None):
    appliedFilter = None
    print "BEGIN"
    
    if format != None:
        if format != 'xml' and format != 'text':
            print "ERROR: format wrong\n"
            sys.exit(1)
    else:
        format = 'text'
        
    if status != None:
        if status != 'all' and status != 'running':
            print "ERROR: status wrong\n"
            sys.exit(1)
    else:
        status = 'running'
    
    if credfilename != None:
        managevm.credfilename = credfilename;
    else:
        managevm.credfilename = 'credentials_myproject.cred';
        
    awsSettings = managevm.AWSSettings(managevm.credfilename)
    mgrEC2 = managevm.ManagerEC2(awsSettings, proxy)

    if not mgrEC2:
       print "ERROR: Conecting to EC2\n"
       sys.exit(1)
    
    if vmfilter:
        print "Using filter: %s\n" % vmfilter
        vmfilter = '.*' + vmfilter + '.*'
        appliedFilter = managevm.OpenCloudVMFilter(vmfilter)

    vmXMLRoot = ET.Element('VirtualMachine')
    vmList = mgrEC2.getVMList(appliedFilter)
    
    if vmList:
        rawvmlist = vmList.getList()
        
        for vm in rawvmlist:
            if status == 'running':
                if vm.instance.state != 'pending' and vm.instance.state != 'running':
                    continue
                    
            attr = {'id': vm.getID(), 'name': vm.getName(), 'state': vm.instance.state, 'statecode': str(vm.instance.state_code)}
            ET.SubElement(vmXMLRoot, 'Instance', attr)
            if format == 'text':
                print "Instance ID: %s" % vm.getID()
                print "Instance Name: %s" % vm.getName()
                print "State: %s" % vm.instance.state
                print "State code: %s" % vm.instance.state_code
                print "Instance Public IP: %s" % vm.getPublicIP()
                print "Instance Public DNS: %s" % vm.getPublicDNS()

    if format == 'xml':
        ET.dump(vmXMLRoot)
    
    return managevm.VMListDescription(vmList, vmXMLRoot)


    
if __name__ == "__main__":
    proxy = None
    credfilename = None
    fil = None
    format = None
    status = None
    
    if len(sys.argv) < 6:
        print "wrong number of arguments\n"
        sys.exit(1)
    
    for a in sys.argv:
        print "ARG: %s" % a
    
    if sys.argv[1]:
        proxy = sys.argv[1]
    else:
        print "wrong no proxy settings\n"
        sys.exit(1)
        
    if sys.argv[2]:
        credfilename = sys.argv[2]
        
    if sys.argv[3]:
        fil = sys.argv[3]
    else:
        print "wrong no filter\n"
        sys.exit(1)
        
    if sys.argv[4]:
        format = sys.argv[4]
    else:
        print "wrong no format\n"
        sys.exit(1)

    if sys.argv[5]:
        status = sys.argv[5]
    else:
        print "wrong no status\n"
        sys.exit(1)
        
    execute(proxy, credfilename, fil, format, status)

        
        
        
