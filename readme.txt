These scripts let you start/stop VM's in AWS or any IaaS with AWS-compatible API.
It's also possible to assign Elastic IP's by following a procedure:
The VM needs to have a tag with key 'IPAddress' set to the elastic/public IP to assign.
To manage a VM, you can specify a filter to be used with the name tag.
The scripts work for proxy as well.
The file credentials_myproject.cred shows an example with the configuration needed for credentials and proxy.
The *.bat files show how to use these scripts for basic functionality.

I hope it's useful for you.

Rodolfo - rodolk


